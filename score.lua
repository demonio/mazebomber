local state = {}
local functions = require("functions")
local suit = require 'suit'
state.first = false
state.instance = nil
local score_list = nil
local best = 0
local bg_bg = functions.generateBox(320, 370, {50,50,50,50}, 0)
local bg = functions.generateBox(310, 360, {200,200,200,50}, -75)
local width = 0
local height = 0
local list_width = 0
local list_height = 0

function state.load()
  --get score values
  score_list = nil
  best = 0
  score = functions.recursiveEnumerate("score")
  limit = 1
  for i,v in pairs(score) do
    if not score_list then
      score_list = ""
    end
    local value = love.filesystem.read(v)
    if limit < 5 then
      score_list = score_list..os.date("%x", i)..":"..value.."\n"
    end
    if tonumber(value) > best then
      best = tonumber(value)
    end
    limit = limit + 1
  end
end

function state.update(dt)
   width = love.graphics.getWidth( )
   height = love.graphics.getHeight( )
   tmp_list_width = width/3*2
   tmp_list_height=height/3*2
   if tmp_list_height ~= list_height then
     list_width = width/3*2
     list_height=height/3*2
     bg_bg = functions.generateBox(list_width+20, list_height+20, {50,50,50,50}, 0)
     bg = functions.generateBox(list_width+10, list_height+10, {200,200,200,50}, -75)
   end
   suit.Label("Best:"..best,  (width/2-list_width/2),25, list_width,100)
   if score_list then
     suit.Label(score_list,  (width/2-list_width/2),((height/2)-list_height/2), list_width,list_height)
   end
end

function state.keypressed(key)
   if key == "escape" then
    state.instance = "menu"
  end
end

function state.draw()
  love.graphics.draw(bg_bg, width/2-list_width/2+10,((height/2)-list_height/2)-10)
  love.graphics.draw(bg, width/2-list_width/2+10,((height/2)-list_height/2)-5)
  suit.draw()
end

return state
