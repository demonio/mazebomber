local functions = require("functions")

local game = {}
game.first = false
game.instance = nil

local tile_width_base = 64
local tile_height_base = 64
local tile_width = 64
local tile_height = 64
local font = love.graphics.newFont("font.ttf", tile_height / 1.8 )

local maze = require("maze")
local maze_x_base = 8
local maze_y_base = 8
local maze_x = maze_x_base
local maze_y = maze_x_base
local map = {}
local game_over = false;
local game_over_text = "GAME OVER"
local suit = require "suit"
local level = 1
local timer = 0
local bomb_timer_base = 5
local bomb_timer = bomb_timer_base
local tx = 0
local ty = 0
local width = 0
local height = 0
local images = functions.recursiveEnumerate("images")

local tiles = {}
tiles[1] = images["wall"][tostring(math.random(4))]
tiles[2] = images["box"][tostring(math.random(6))]
tiles[3] = images["exit"]["1"]
tiles[0] = images["ground"][tostring(math.random(4))]
tiles[4] = images["star"][tostring(math.random(5))]

local round_base = 1
local tick_base = 1 / 8
local tick = 0
local round = tick_base
local plus = {}
local button_check = {}
local explosion_icon = images["icons"]["explosion"]
local heart_icon = images["icons"]["heart"]
local button = {}
button.x = images["icons"]["white"]["buttonX"]

local enemy = {}
local enemy_amount = 0
local enemy_move = 4
local enemy_move_base = 4
local best_score = 0
local score = 0

--local ai = require("ai")

local Player = require("player")
local player = {}
player = Player:init()

function reload(x, y)
  local x = x or 0
  local y = y or 0
  maze_x = maze_x + x
  maze_y = maze_y + y
  if x ~= 0 then
    level = level + 1
  end

  if level % 5 == 0 then
    enemy_amount = enemy_amount + 1
  end

  player:reload()

  tiles[1] = images["wall"][tostring(math.random(4))]
  tiles[2] = images["box"][tostring(math.random(6))]
  tiles[3] = images["exit"]["1"]
  tiles[0] = images["ground"][tostring(math.random(4))]

  map = maze.map_maze(maze_x, maze_y, os.time())

  game.load()
end

function reset()
  --print("RESET")
  player:reset()
  level = 1
  maze_x = maze_x_base
  maze_y = maze_x_base

  map = maze.map_maze(maze_x, maze_y, os.time())

  game.over = false
end

function game.load()
  --print(game.first)
  if not game.first then
    reset()
  end
  love.graphics.setFont(font)
  local mapa = map.map
  for x = 1, map.width do
    for y = 1, map.height do
      if (x == 1 or y == 1 or x == map.width or y == map.height) then
        if mapa[x][y] == 0 then
          if player.x == 0 and player.y == 0 then

            for x1 = x - 1, x + 1 do
              for y1 = y - 1, y + 1 do
                if mapa[x1] then
                  if mapa [x1][y1] == 0 and x1 ~= x and y1 ~= y then
                    player.x = x1
                    player.y = y1
                    mapa[x][y] = 1
                  end
                end
              end
            end

          end
          if mapa[x][y] == 0 then mapa[x][y] = 3 end
        end
      end
    end
  end

  player:set_position(map)
  map = player:clean(map)
  for i = 1, enemy_amount do
    enemy[i] = Player:init(true)
    enemy[i]:set_position(map)
    map = enemy[i]:clean(map)
  end
  --print(functions.tableToText(map))
  --player.x = math.random(1,map.width)
  --player.y = math.random(1,map.height)
end

function game.update(dt)
  if not game.first then
    return
  end
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  if tile_height ~= tile_height_base * scale then
    font = love.graphics.newFont("font.ttf", tile_height_base * scale / 2.5)
    love.graphics.setFont(font)
  end
  tile_width = tile_width_base * scale
  tile_height = tile_height_base * scale
  --print(game_over)
  if game_over then
    return
  end
  if player.points <= 0 then
    game_over = true
  end
  tmp_button = suit.ImageButton(button.x, {draw = functions.draw, scale = scale},
  width - button.x:getWidth() * 1.5 * scale, height - button.x:getHeight() * 1.5 * scale)
  if tmp_button.hovered then
    if player.max_bombs > 0 then
      for i, v in ipairs(player.bomb) do
        if v.x == player.x and v.y == player.y then
          return
        end
      end
      table.insert(player.bomb, {x = player.x, y = player.y, timer = player.timer})
      player.max_bombs = player.max_bombs - 1
    end
    return
  else
    player:update(dt, map)
    for i = 1, enemy_amount do
      enemy[i]:update(dt, map)
    end
  end

  tx = width / 2 - tile_width / 2
  ty = height / 2 - tile_height / 2
  tx = player.act_x * tile_width - tx
  ty = player.act_y * tile_width - ty
  if tick > 0 then
    tick = tick - dt
  else
    if player.lives <= 0 then
      love.filesystem.write("score/"..os.time(), (level * 100) + player.points)
      game_over = true
    end
    player:tick(map, enemy)
    if enemy_move == 0 then
      enemy_move = enemy_move_base
      for i = 1, enemy_amount do
        tmp = {player}
        if not enemy[i].dead then
          enemy[i]:tick(map, tmp)
        end
      end
    else
      enemy_move = enemy_move - 1
    end
    --end
    tick = tick_base
  end

  if round > 0 then
    round = round - dt
  else
    --TODO round done do stuff
    player:round(map, round_base)
    round = round_base
  end
end

function game.keypressed(key, scancode, isrepeat)
  if scancode and game_over then
    game.instance = "menu"
    game_over = false
    reset()
  end
  if scancode == "space" then
    if player.max_bombs > 0 then
      table.insert(player.bomb, {x = player.x, y = player.y, timer = player.timer})
      player.max_bombs = player.max_bombs - 1
    end
  end
  if scancode == "escape" then
    game.instance = "menu"
  end
end

function game.draw()
  --local font = love.graphics.getFont()
  local mapa = map.map
  for x = 1, map.width do
    for y = 1, map.height do
      love.graphics.draw(tiles[0], (x * tile_width) - tx, (y * tile_height) - ty, 0, scale, scale)
      if mapa[x][y] ~= 0 then
        love.graphics.draw(tiles[mapa[x][y]], (x * tile_width) - tx, (y * tile_height) - ty, 0, scale, scale)
      end
    end
  end
  player:draw(tx, ty, tile_width, tile_height, scale)
  for i = 1, enemy_amount do
    enemy[i]:draw(tx, ty, tile_width, tile_height, scale)
  end
  love.graphics.print(player.points, width / 2 - font:getWidth(player.points) / 2, 10)

  --DEBUG
  --love.graphics.print(level, width / 2 - font:getWidth(player.points) / 2, 10 + tile_height)
  suit.draw()
  if game_over then
    love.graphics.print(game_over_text, width / 2 - font:getWidth(game_over_text) / 2, height / 2 - font:getHeight() / 2)
  end
end

return game
