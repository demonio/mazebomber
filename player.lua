local functions = require("functions")
require("class")
local images = functions.recursiveEnumerate("images")
local srdce = images["hud"]["srdce"]
local bomb = images["bomb"]["1"]
local effects = {}
local color = {"yellow", "red", "orange", "black"}
local explosion_sound = love.audio.newSource( "audio/explosion2.ogg", "static" )
effects.explode = images["explosion"][color[math.random(#color)]]
local ai = require "ai"

local player = class()

function player:init(ai)
  local tmp_player = {}
  setmetatable(tmp_player, player)

  tmp_player.image = images["player"]
  tmp_player.x = 0
  tmp_player.y = 0
  tmp_player.old_x = -1
  tmp_player.old_y = -1
  tmp_player.level = 0
  tmp_player.act_y = 0
  tmp_player.act_x = 0
  tmp_player.speed = 8
  tmp_player.radius = 2
  tmp_player.timer = 3
  tmp_player.dead = false
  if ai then
    tmp_player.lives = 1
    tmp_player.max_bombs = 0
    tmp_player.max_bombs_base = 0
  else
    tmp_player.lives = 3
    tmp_player.max_bombs = 2
    tmp_player.max_bombs_base = 2
  end
  tmp_player.anim = 1
  tmp_player.anim_max = 3
  tmp_player.anim_side = "bot"
  tmp_player.bomb = {}
  tmp_player.effect = {}
  tmp_player.points = 50
  tmp_player.ai = ai
  tmp_player.price = {
    bomb = 50,
    explosion = 75,
    live = 125
  }
  tmp_player.mouse = {
    timer = 0,
    timer_base = 1 / 8
  }

  return tmp_player
end

function player:set_position(map)
  if map.map[self.x] == nil then
    self.x = math.random(1, map.width)
  end

  if map.map[self.x][self.y] == nil then
    self.y = math.random(1, map.height)
  end

  while map.map[self.x][self.y] > 0 do
    self.x = math.random(1, map.width)
    self.y = math.random(1, map.height)
  end
  --print(self.x,self.y)
end

function player:reload()
  self.image = images["player"]
  self.x = 0
  self.y = 0
  self.bomb = {}
  self.effect = {}
  self.max_bombs = self.max_bombs_base
  effects.explode = images["explosion"][color[math.random(#color)]]
end

function player:reset(ai)
  local ai = ai or false
  self.image = images["player"]
  self.x = 0
  self.y = 0
  self.level = 0
  self.act_y = 0
  self.act_x = 0
  self.speed = 8
  self.radius = 2
  self.timer = 3
  self.dead = false
  if ai then
    self.lives = 1
    self.max_bombs = 0
    self.max_bombs_base = 0
  else
    self.lives = 3
    self.max_bombs = 2
    self.max_bombs_base = 2
  end
  self.anim = 1
  self.anim_max = 3
  self.anim_side = "bot"
  self.bomb = {}
  self.effect = {}
  self.points = 50
  self.ai = ai
  self.price = {
    bomb = 50,
    explosion = 75,
    live = 125
  }
end

local function distance ( x1, y1, x2, y2 )
  local dx = x1 - x2
  local dy = y1 - y2
  return math.sqrt ( dx * dx + dy * dy )
end

function player:clean(map)
  for x = self.x - 2, self.x + 2 do
    for y = self.y - 2, self.y + 2 do
      if map.map[x] then
        if map.map[x][y] == 2 then
          map.map[x][y] = 0
          map[x][y].number = 0
        end
      end
    end
  end
  return map
end

function player:update(dt, map)
  if self.lives < 1 then
    self.dead = true
  end

  if self.dead then
    return
  end

  self.act_y = self.act_y - ((self.act_y - self.y) * self.speed * dt)
  self.act_x = self.act_x - ((self.act_x - self.x) * self.speed * dt)

  if not self.ai then
    if self.mouse.timer > 0 then
      self.mouse.timer = self.mouse.timer - dt
    else
      if love.mouse.isDown(1) then
        local touches = love.touch.getTouches()
        local x = 0
        local y = 0
        for i, id in ipairs(touches) do
          x, y = love.touch.getPosition(id)
        end
        local dir = {{x = -1, y = 0, move = "left"}, {x = 1, y = 0, move = "right"}, {x = 0, y = -1, move = "top"}, {x = 0, y = 1, move = "bot"}}
        local c_x = width / 2
        local c_y = height / 2
        local base_dist = distance(c_x, c_y, x, y)
        local tmp_dir = 1
        for i, v in pairs(dir) do
          local tmp_dist = distance((c_x + v.x), (c_y + v.y), x, y)
          if base_dist > tmp_dist then
            base_dist = tmp_dist
            tmp_dir = i
          end
        end
        self:move(map, dir[tmp_dir].x, dir[tmp_dir].y, dir[tmp_dir].move)
        self.mouse.timer = self.mouse.timer_base
      end
    end
  end
end

function player:tick(map, enemy)
  if self.dead then
    return
  end


  for i, v in pairs(enemy) do
    for a, b in pairs(self.effect) do
      if v.x == b.x and b.y == v.y then
        v.lives = v.lives - 1
      end
    end
    if v.x == self.x and v.y == self.y and not v.dead then
      v.lives = v.lives - 1
      self.lives = self.lives - 1
    end
  end

  for i, v in pairs(self.effect) do
    if v.x == self.x and v.y == self.y then
      if self.lives > 0 then
        self.lives = self.lives - 1
      end
    end
  end
  for i, v in ipairs(self.effect) do
    if v.tick > 0 then
      v.tick = v.tick - 1
    else
      table.remove(self.effect, i)
    end
  end

  if self.ai == false then
    if love.keyboard.isDown("w")
    or love.keyboard.isDown("up") then
      self:move(map, 0, - 1)
      self.anim_side = "top"
      return true
    end
    if love.keyboard.isDown("a")
    or love.keyboard.isDown("left") then
      self:move(map, - 1, 0)
      self.anim_side = "left"
      return true
    end
    if love.keyboard.isDown("s")
    or love.keyboard.isDown("down") then
      self:move(map, 0, 1)
      self.anim_side = "bot"
      return true
    end
    if love.keyboard.isDown("d")
    or love.keyboard.isDown("right") then
      self:move(map, 1, 0)
      self.anim_side = "right"
      return true
    end
  else
    for i, v in pairs(enemy) do
      tmp = ai.move(map, v, self)
    end
    self:move(map, tmp.x, tmp.y)
  end
  return false
end

function player:insert_effect(map, x, y, tick)
  if not map.map[x] then return false end
  if not map.map[x][y] then return false end
  if map.map[x][y] == 1 then
    return false
  end
  table.insert(self.effect, {effect = "explode", x = x, y = y, tick = (self.radius - tick)})
  if map.map[x][y] == 2 then
    map.map[x][y] = 0
    map[x][y].number = 0
    return false
  end
  return true
end

function player:round(map, round_base)
  --self.points = self.points - 1
  for i, v in ipairs(self.bomb) do
    if v.timer > 0 then
      v.timer = v.timer - round_base
    else
      love.audio.play(explosion_sound)
      if love.system.getOS() == "Android" then
        love.system.vibrate( 0.5 )
      end
      for x = 0, self.radius do
        tmp_x = v.x + x
        if not self:insert_effect(map, tmp_x, v.y, x) then break end
      end
      for x = 0, - self.radius, - 1 do
        tmp_x = v.x + x
        if not self:insert_effect(map, tmp_x, v.y, - x) then break end
      end
      for y = 0, self.radius do
        tmp_y = v.y + y
        if not self:insert_effect(map, v.x, tmp_y, y) then break end
      end
      for y = 0, - self.radius, - 1 do
        tmp_y = v.y + y
        if not self:insert_effect(map, v.x, tmp_y, - y) then break end
      end
      table.remove(self.bomb, i)
      self.max_bombs = self.max_bombs + 1
    end
  end
end

function player:move(map, x, y, side)
  if not map.map[self.x + x] then
    reload()
  end
  if not map.map[self.x + x] then
    return
  end
  if not map.map[self.x + x][self.y + y] then
    return
  end
  if map.map[self.x + x][self.y + y] == 4 then
    self.points = self.points + 10
    map.map[self.x + x][self.y + y] = 0
    map[self.x + x][self.y + y].number = 0
  end
  if map.map[self.x + x][self.y + y] == 3 then
    self.level = self.level + 1
    self.points = self.points + 10 * self.level
    reload(2, 2)
  end
  if map.map[self.x + x] then
    if map.map[self.x + x][self.y + y] == 0 then
      self.x = self.x + x
      self.y = self.y + y
    end
  end
  self.anim = self.anim + 1
  if self.anim > self.anim_max then
    self.anim = 1
  end
  if self.x ~= self.old_x or self.y ~= self.old_y then
    self.points = self.points - 1
  end
  self.old_x = self.x
  self.old_y = self.y
  if side then
    self.anim_side = side
  end
end

function player:draw(tx, ty, tile_width, tile_height, scale, rotation)
  local rotation = rotation or 0
  if self.dead then
    return
  end
  local tx = tx or 0
  local ty = ty or 0
  local tile_width = tile_width or 64
  local tile_height = tile_height or 64
  local scale = scale or 1
  for i, v in ipairs(self.bomb) do
    local scale = scale + (math.random() * 0.1)
    love.graphics.draw(bomb, (v.x * tile_width) - tx, (v.y * tile_height) - ty, rotation, scale, scale)
    love.graphics.setColor(255, 0, 0)
    love.graphics.print(v.timer, (v.x * tile_width) - tx, (v.y * tile_height) - ty, rotation, scale, scale)
    love.graphics.setColor(255, 255, 255)
  end
  for i, v in ipairs(self.effect) do
    love.graphics.draw(effects[v.effect][tostring(4 - v.tick)], (v.x * tile_width) - tx, (v.y * tile_height) - ty, rotation, scale, scale)
  end
  local tmp_x = self.act_x * tile_width - tx
  local tmp_y = self.act_y * tile_height - ty
  if rotation ~= 0 then
    for i = 1, self.lives do
      love.graphics.draw(srdce, (tmp_x + self.image[self.anim_side][tostring(self.anim)]:getWidth() * scale / 2) - srdce:getWidth() * scale, tmp_y + (self.lives / 2 * srdce:getHeight() * scale) - ((i - 2) * srdce:getHeight() * scale), rotation, scale, scale)
    end
    for i = 1, self.max_bombs do
      love.graphics.draw(bomb, (tmp_x + self.image[self.anim_side][tostring(self.anim)]:getWidth() * scale / 2) + srdce:getWidth() * scale * 0.5, tmp_y + (self.max_bombs / 2 * srdce:getHeight() * scale) - ((i - 2) * srdce:getHeight() * scale), rotation, scale / 4, scale / 4)
    end
  else
    for i = 1, self.lives do
      love.graphics.draw(srdce, (tmp_x + self.image[self.anim_side][tostring(self.anim)]:getWidth() * scale / 2) - self.lives / 2 * srdce:getWidth() * scale + ((i - 1) * srdce:getWidth() * scale), tmp_y - srdce:getHeight() * scale, rotation, scale, scale)
    end
    for i = 1, self.max_bombs do
      love.graphics.draw(bomb, (tmp_x + self.image[self.anim_side][tostring(self.anim)]:getWidth() * scale / 2) - self.max_bombs / 2 * srdce:getWidth() * scale + ((i - 1) * srdce:getWidth() * scale), tmp_y - srdce:getHeight() * scale * 2, rotation, scale / 4, scale / 4)
    end
  end
  if self.ai then
    love.graphics.setColor(255, 100, 100)
  end
  love.graphics.draw(self.image[self.anim_side][tostring(self.anim)], tmp_x, tmp_y, rotation, scale, scale)
  love.graphics.setColor(255, 255, 255)
end

return player
