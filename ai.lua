local ai = {}

local functions = require("functions")

local function swap(array, index1, index2)
   array[index1], array[index2] = array[index2], array[index1]
end

local function distance ( x1, y1, x2, y2 )
   local dx = x1 - x2
   local dy = y1 - y2
   return math.sqrt ( dx * dx + dy * dy )
end

local function shuffle(array)
   local counter = #array
   while counter > 1 do
      local index = math.random(counter)
      swap(array, index, counter)
      counter = counter - 1
   end
end

local function map_draw(map,layer)
	local write = io.write
	for y=1,map.height do
		for x=1,map.width do
			write(map[x][y].number)
		end
		write("\n")
	end
end

local function map_clean(map,walk,air)
   local air = air or " "
   local walk = walk or "0"
   --print(air,walk)
   --print(map.width,map.height)
   for x=1,map.width do
      for y=1,map.height do
  --print(x,y)
      	 if map[x][y].number == walk then
      	    map[x][y].number = air
      	 end
         map[x][y].visited = false
      end
   end
end

local function smart_solve(map, x, y, kx,ky, walk,air,solved)
   local steps = 0
   local walk = walk or "9"
   local solved = solved or false
   local air = air or " "
   map[x][y].visited = true
   map[x][y].number = walk
   if x == kx and y == ky then
      return steps+1
   end
   local dir = {{x=-1,y=0,move="left"},{x=1,y=0,move="right"},{x=0,y=-1,move="up"},{x=0,y=1,move="down"}}
   local direction = math.random(#dir)

   if kx == nil or ky == nil then
      return 0
   end

   if kx < x then
      direction = 1
   elseif kx > x then
      direction = 2
   elseif ky < y then
      direction = 3
   else
      direction = 4
   end

   while #dir > 0 do
      local move_x = x+dir[direction].x
      local move_y = y+dir[direction].y
      local move = dir[direction].move
      table.remove(dir, direction)
      if move_x <= map.width and move_x >= 1 and move_y >= 1 and move_y <= map.height then
	 if (not map[move_x][move_y].visited and map[move_x][move_y].number == air ) then
	    steps = smart_solve(map,move_x,move_y,kx,ky,walk,air)
	 end
      end
      if steps > 0 then
	 return steps+1
      end
      if #dir > 0 then
	 --shuffle(dir)
	 local tmp_dir = nil
	 local base_dist = distance(x,y,kx,ky)
	 for i,v in pairs(dir) do
	    local tmp_dist = distance((x+v.x),(y+v.y),kx,ky)
	    if base_dist > tmp_dist then
	       base_dist = tmp_dist
	       tmp_dir = i
	    end
	 end
	 direction= tmp_dir or math.random(#dir)
      end
   end

   map[x][y].number = air

   return 0
end

function ai.locate(player,id)

end

function ai.remove_box(x,y)
  ai.map[x][y].number = 0
end

function ai.move(map,enemy,player)
  local x = 0
  local y = 0
  local tmp = nil
  local dir = {{x=-1,y=0,move="left"},{x=1,y=0,move="right"},{x=0,y=-1,move="up"},{x=0,y=1,move="down"}}

  --os.execute("clear")
  --print(map, player.x, player.y, enemy.x,enemy.y, 9,0)
  local tmp = smart_solve(map, player.x, player.y, enemy.x,enemy.y, 9,0)

  for x=1,map.width do
     for y=1,map.height do
       map[x][y].visited = false
     end
  end

      --map_draw(map)
      --print(tmp)
      if tmp > 1 then
        for a,b in pairs(dir) do
          if map[player.x+b.x][player.y+b.y].number == 9 then
            x = b.x
            y = b.y
            break
        end
      end
      --map_draw(ai.map)
      map_clean(map,9,0)
    end

  if map[player.x + x][player.y + y].number == 0 then
    return {x=x,y=y}
  end
  return {x=0,y=0}
end

return ai
