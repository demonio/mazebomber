local state = {}
state.first = false
state.instance = nil
local functions = require("functions")
local icons = functions.recursiveEnumerate("images/icons")
--TODO fix email in credits
local credits = love.graphics.newImage("credits.png")
local kenney = love.graphics.newImage("kenney.png")
local default_font = love.graphics.newFont("font.ttf", 64 )
local width = 0
local height = 0
local button = {}
local active = 1
local active_max = 4
local suit = require "suit"
local name = "Maze Bomber"
button.single = {
   id = 1,
   img = icons["white"]["gamepad"],
   x=-1.5,
   y=-0.5,
   link = "game"
}
--button.multi = {
--   id = 2,
--   img = icons["white"]["multiplayer"],
--   x=0.5,
--   y=-1,
--   link = "ai_game"
--}
--button.options = {
--   id = 2,
--   img = icons["white"]["wrench"],
--   x=0,
--   y=-1,
--   link = "menu"
--}
button.score = {
   id = 3,
   img = icons["white"]["trophy"],
   x=-0.5,
   y=-0.5,
   link = "score"
}
--[[
button.exit = {
   id = 4,
   img = icons["white"]["exit"],
   x=0.5,
   y=-0.5,
   link = "quit"
}
local bt_bg = functions.generateBox(100,100,{255,50,50},-50)
--]]

function state.load()
  default_font = love.graphics.newFont("font.ttf", 64*scale )
  love.graphics.setFont(default_font)
end

function state.update(dt)
   width = love.graphics.getWidth( )
   height = love.graphics.getHeight( )
   for i,v in pairs(button) do
     if suit.ImageButton(v.img,
        {draw=functions.draw,scale=scale},
        (width/2)+(v.img:getWidth()*scale*v.x),(height/2)+(v.img:getHeight()*scale*v.y)).hit then
       state.instance = v.link
     end
   end
end

function state.draw()
  if not state.first then
    return
  end
  local font = love.graphics.getFont()
  love.graphics.print(name,width/2-font:getWidth(name)/2,10)
  --for i,v in pairs(button) do
    --if active == v.id then
      --love.graphics.draw(bt_bg, (width/2)+(v.img:getWidth()*v.x),(height/2)+(v.img:getHeight()*v.y))
    --end
    --love.graphics.draw(v.img, (width/2)+(v.img:getWidth()*v.x),(height/2)+(v.img:getHeight()*v.y))
  --end
  suit.draw()
  love.graphics.draw(credits,width-credits:getWidth(),height-credits:getHeight())
  love.graphics.draw(kenney,10,height-kenney:getHeight())
end

function state.textinput(t)

end

function state.keypressed(key)
  if key == "up" or key == "w" then
    active = active - 1
    if active == 0 then
      active = active_max
    end
  end

  if key == "down" or key == "s" then
      active = active + 1
      if active > active_max then
        active = 1
      end
  end

  if key == "return" then
    for i,v in pairs(button) do
      if v.id == active then
        if v.link then
          state.instance = v.link
        end
      end
    end
  end
  if key == "escape" then
    state.instance = "quit"
  end
end

function state.mousepressed(x, y, button)

end

return state
