local functions = require("functions")
local ai = require("ai")

local game = {}
game.first = false
game.instance = nil

local tile_width = 64
local tile_height = 64
local font = love.graphics.newFont( tile_height )

local maze = require("maze")
local maze_x_base = 9
local maze_y_base = 9
local maze_x = maze_x_base
local maze_y = maze_x_base
local map = {}
map, ai.map = maze.map_bomber(maze_x,maze_y,os.time())


local level = 1
local timer = 0
local tx = 0
local ty = 0
local width = 0
local height = 0
local images = functions.recursiveEnumerate("images")

local tiles = {}
tiles[1] = images["wall"][tostring(math.random(4))]
tiles[2] = images["box"][tostring(math.random(6))]
tiles[3] = images["exit"]["1"]
tiles[0] = images["ground"][tostring(math.random(4))]
tiles[4] = images["star"][tostring(math.random(5))]
local srdce = images["hud"]["srdce"]

local round_base = 1
local tick_base = 1/8
local tick = 0
local round = tick_base
local color = {"yellow", "red", "orange", "black"}
local bomb = functions.generateBox(tile_width,tile_height,{200,50,50},-50)
local effects = {}
effects.explode = images["explosion"][color[math.random(#color)]]

local spawn = {}

local player = {}
player[1] = {
  id = 1,
   image =  images["player"],
   x = 0,
   y = 0,
   act_y = 0,
   act_x = 0,
   speed = 8,
   radius = 2,
   timer = 3,
   lives = 3,
   max_bombs = 2,
   anim = 1,
   anim_max = 3,
   anim_side = "bot",
   bomb = {},
   effect = {},
   points = 0
}

local function reload()

  for i,v in pairs(player) do
   v.image =  images["player"]
   v.x = 0
   v.y = 0
   v.bomb = {}
   v.effect = {}
 end


   tiles[1] = images["wall"][tostring(math.random(4))]
   tiles[2] = images["box"][tostring(math.random(6))]
   tiles[3] = images["exit"]["1"]
   tiles[0] = images["ground"][tostring(math.random(4))]
   effects.explode = images["explosion"][color[math.random(#color)]]

   map, ai.map = maze.map_bomber(maze_x,maze_y,os.time())

   game.load()
end

local function move(x,y)
   if not map.map[player[1].x+x] then
      reload()
   end
   if map.map[player[1].x+x][player[1].y+y] == 4 then
      player[1].points = player[1].points + 1
      map.map[player[1].x+x][player[1].y+y] = 0
      map[player[1].x+x][player[1].y+y].number = 0
   end
   if map.map[player[1].x+x][player[1].y+y] == 3 then
      level = level + 1
      player[1].points = player[1].points + 2*level
      maze_x = maze_x + 2
      maze_y = maze_y + 2
      reload()
   end
   if map.map[player[1].x+x] then
      if map.map[player[1].x+x][player[1].y+y] == 0 then
	 player[1].x = player[1].x + x
	 player[1].y = player[1].y + y
      end
   end
   player[1].anim = player[1].anim + 1
   if player[1].anim > player[1].anim_max then
      player[1].anim = 1
   end
   ai.map = map
end

function game.load()
   love.graphics.setFont(font)
   local mapa = map.map
   local spawn = {{2,2},{map.width-1,map.height-1},{2,map.height-1},{map.width-1,2}}
   local enemy = functions.round(level/3)+1

   for i=1,enemy do
     player[i+1] ={
       id = i+1,
        image =  images["player"],
        x = 0,
        y = 0,
        act_y = 0,
        act_x = 0,
        speed = 8,
        radius = 2,
        timer = 3,
        lives = 3,
        max_bombs = 2,
        anim = 1,
        anim_max = 3,
        anim_side = "bot",
        bomb = {},
        effect = {},
        points = 0
     }
   end

   for i,v in pairs(player) do
     local tmp = math.random(#spawn)
     local player_spawn = spawn[tmp]
     table.remove(spawn,tmp)

     v.x = player_spawn[1]
     v.y = player_spawn[2]

     for x=v.x-2,v.x+2 do
        for y=v.y-2,v.y+2 do
        if mapa[x] then
           if mapa[x][y] == 2 then
              mapa[x][y] = 0
              if ai.remove_box then ai.remove_box(x,y) end
           end
        end
            end
         end
       end
    end

function game.update(dt)
  for i,v in pairs(player) do
    v.act_y = v.act_y - ((v.act_y - v.y) * v.speed * dt)
    v.act_x = v.act_x - ((v.act_x - v.x) * v.speed * dt)
  end
   width = love.graphics.getWidth( )
   height = love.graphics.getHeight( )
   tx = width/2-tile_width/2
   ty = height/2-tile_height/2
   tx = player[1].act_x*tile_width-tx
   ty = player[1].act_y*tile_width-ty
   if tick > 0 then
      tick = tick - dt
   else
     for a,b in pairs(player) do
       for c,d in pairs(player) do
          for i,v in pairs(d.effect) do
            if v.x == b.x and v.y == b.y then
              if b.lives > 0 then
                b.lives = b.lives - 1
              else
                if a ~= 1 then
                  table.remove(player,a)
                else
                  love.filesystem.write("score/"..os.time(), player[1].points)
                  game.instance = "menu"
                end
              end
            end
          end
       end
     end
     for a,b in pairs(player) do
       if a ~= 1 then
         if ai.move then ai.move(player,a) end
       end
     end
      if love.keyboard.isDown("w") or love.keyboard.isDown("up") then
	 move(0,-1)
         player[1].anim_side = "top"
      end
      if love.keyboard.isDown("a") or love.keyboard.isDown("left") then
	 move(-1,0)
         player[1].anim_side = "left"
      end
      if love.keyboard.isDown("s") or love.keyboard.isDown("down") then
	 move(0,1)
         player[1].anim_side = "bot"
      end
      if love.keyboard.isDown("d") or love.keyboard.isDown("right") then
	 move(1,0)
         player[1].anim_side = "right"
      end

      for a,b in pairs(player) do
        for i,v in ipairs(b.effect) do
  	 if v.tick > 0 then
  	    v.tick = v.tick - 1
  	 else
  	    table.remove(b.effect,i)
  	 end
        end
      end

      for i,v in pairs(player) do
        if i ~= 1 then
          if ai.locate then ai.locate(player,v.id) end
        end
      end
      if #player ==  -1 then
        level = level + 1
        player[1].points = player[1].points + 4*level
        maze_x = maze_x + 2
        maze_y = maze_y + 2
        reload()
      end
      tick = tick_base
   end

   if round > 0 then
      round = round - dt
   else
      --TODO round done do stuff
      round = round_base
      for a,b in ipairs(player) do
      for i,v in ipairs(b.bomb) do
	 local map = map.map
	 if v.timer > 0 then
	    v.timer = v.timer - round_base
	 else
	    for x=0,b.radius do
	       tmp_x = v.x+x
	       if map[tmp_x][v.y] == 1 then
		  break
	       end
	       table.insert(b.effect, {effect="explode",x=tmp_x,y=v.y,tick=(b.radius-x)})
	       if map[tmp_x][v.y] == 2 then
		         map[tmp_x][v.y] = 0
      if ai.remove_box then ai.remove_box(tmp_x,v.y) end
		  break
	       end
	    end
	    for x=0,-b.radius,-1 do
	       tmp_x = v.x+x
	       if map[tmp_x][v.y] == 1 then
		  break
	       end
	       table.insert(b.effect, {effect="explode",x=tmp_x,y=v.y,tick=(b.radius+x)})
	       if map[tmp_x][v.y] == 2 then
		  map[tmp_x][v.y] = 0
      if ai.remove_box then ai.remove_box(tmp_x,v.y) end
		  break
	       end
	    end
	    for y=0,b.radius do
	       tmp_y = v.y+y
	       if map[v.x][tmp_y] == 1 then
		  break
	       end
	       table.insert(b.effect, {effect="explode",x=v.x,y=tmp_y,tick=(b.radius-y)})
	       if map[v.x][tmp_y] == 2 then
		  map[v.x][tmp_y] = 0
      if ai.remove_box then ai.remove_box(v.x,tmp_y) end
		  break
	       end
	    end
	    for y=0,-b.radius,-1 do
	       tmp_y = v.y+y
	       if map[v.x][tmp_y] == 1 then
		  break
	       end
	       table.insert(b.effect, {effect="explode",x=v.x,y=tmp_y,tick=(b.radius+y)})
	       if map[v.x][tmp_y] == 2 then
		  map[v.x][tmp_y] = 0
      if ai.remove_box then ai.remove_box(v.x,tmp_y) end
		  break
	       end
	    end
	    table.remove(b.bomb, i)
	    b.max_bombs = b.max_bombs + 1
	 end
      end
    end
   end
end

function game.keypressed(key, scancode, isrepeat)
   if scancode == "space" then
      if player[1].max_bombs > 0 then
	 table.insert(player[1].bomb, {x=player[1].x, y=player[1].y, timer=player[1].timer})
	 player[1].max_bombs = player[1].max_bombs - 1
      end
   end
   if scancode == "r" then
      reload()
   end
end

function game.draw()
   local mapa = map.map
   for x=1, map.width do
      for y=1, map.height do
	 love.graphics.draw(tiles[0],(x*tile_width)-tx,(y*tile_height)-ty)
	 if mapa[x][y] ~= 0 then
	    love.graphics.draw(tiles[mapa[x][y]],(x*tile_width)-tx,(y*tile_height)-ty)
	 end
      end
   end

   for a,b in pairs(player) do
   for i,v in ipairs(b.bomb) do
      love.graphics.draw(bomb,(v.x*tile_width)-tx,(v.y*tile_height)-ty)
      love.graphics.print(v.timer,(v.x*tile_width)-tx,(v.y*tile_height)-ty)
   end
   for i,v in ipairs(b.effect) do
      love.graphics.draw(effects[v.effect][tostring(4-v.tick)],(v.x*tile_width)-tx,(v.y*tile_height)-ty)
   end
   local tmp_x = b.act_x*tile_width-tx
   local tmp_y = b.act_y*tile_height-ty
   for i=1,b.lives do
      love.graphics.draw(srdce, (tmp_x + b.image[b.anim_side][tostring(b.anim)]:getWidth()/2) - b.lives/2 * srdce:getWidth() + ((i-1)*srdce:getWidth()) , tmp_y - 20)
   end
   love.graphics.draw(b.image[b.anim_side][tostring(b.anim)],tmp_x,tmp_y)
 end
end

return game
