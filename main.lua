local language = {}
local instance = "menu"
local player = {}
local love = love
local tostring = tostring
local string = string
local functions = require "functions"
local gameinstance = require 'states'
local default_font = love.graphics.newFont("font.ttf", 32 )
local toggle = false
local scale_base = 1920 + 1080
local framerate = 30
local next_time = 0
scale = 1

love.filesystem.write( "venca-test", "this is test please write it" )

function love.load()
  love.graphics.setFont(default_font)
  love.filesystem.createDirectory( "score" )
  next_time = love.timer.getTime()
end

function love.update(dt)
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  next_time = next_time + 1 / framerate
  if love.system.getOS() == "Android" then
    scale = ((width + height) / scale_base) * 2
  end
  if not gameinstance[instance].first then
    gameinstance[instance].instance = instance
    gameinstance[instance].player = player
    if gameinstance[instance].load then gameinstance[instance].load() end
    gameinstance[instance].first = true
  end
  if gameinstance[instance].first and gameinstance[instance].update then
    if gameinstance[instance].updater then
      player = gameinstance[instance].player
      gameinstance[instance].updater = false
    end
    if gameinstance[instance].instance ~= instance then
      instance = gameinstance[instance].instance
      gameinstance[instance].first = false
    end
    gameinstance[instance].update(dt)
  end
  --love.graphics.setFont(default_font)
  --print(gameinstance[instance].instance)
end

function love.draw()
  if gameinstance[instance].first and gameinstance[instance].draw then gameinstance[instance].draw() end
  --love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
  --local stats = love.graphics.getStats()

  --local str = string.format("Memory used: %.2f MB", stats.texturememory / 1024 / 1024)
  --love.graphics.print(str, 10, 40)
  local cur_time = love.timer.getTime()
  if next_time <= cur_time then
    next_time = cur_time
    return
  end
  love.timer.sleep(next_time - cur_time)
end

function love.textinput(t)
  if gameinstance[instance].textinput then gameinstance[instance].textinput(t) end
end

function love.keypressed(key, scancode, isrepeat)
  if key == "f11" then
    toggle = not toggle
    love.window.setFullscreen(toggle, "desktop")
  end
  if gameinstance[instance].keypressed then gameinstance[instance].keypressed(key, scancode, isrepeat) end
end

function love.mousepressed(x, y, button)
  if gameinstance[instance].mousepressed then gameinstance[instance].mousepressed(x, y, button) end
end
